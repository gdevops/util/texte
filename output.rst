Ce lundi commencera la fête de Pessah, la « Pâques juive ».

Nous célébrerons pendant une semaine la fin de l’esclavage de nos ancêtres,
les Hébreux, par les Pharaons d’Égypte. C’est l’émancipation de
notre peuple que nous commémorons : réclamons à l’occasion de cette fête
l’émancipation de tous les peuples.

Cette fête, entre tradition, rituels et transmission de notre Histoire, se
termine par cette phrase : « l’an Prochain à Jérusalem ». Quelles que
soient nos relations avec Israël, cette année plus que jamais le destin des
Juifves en diaspora et celui des israélienn·es ont été liés : une vague
d’antisémitisme terrifiante partout dans le monde a suivi le massacre du
7 octobre et nous en subissons encore les conséquences désastreuses. Comme
le veut la coutume, nous garderons une place vide à la table du Seder pour
le prophète Éli, mais cette année elle sera aussi pour les otages.

Ajoutons que cette fête est aussi un moment intense pour les femmes juives,
à qui il incombe souvent la lourde charge du « ménage de Pessah »,
qui, prétextant une recherche assidue du hamets, impose un nettoyage de
toute la maison de fond en comble. Rappelons notre souhait d’une société
plus équitable et d’une répartition égalitaire des tâches ménagères
qu’aucune fête ne devrait mettre en péril.

Pour une société plus juste et dans laquelle la liberté et l’émancipation
de toustes passe aussi par celle des femmes dans le foyer !

 Hag Pessah Sameah !
